package it.uniroma2.sdcc.flink;

import it.uniroma2.sdcc.flinkUtil.AverageProcessFunction;
import it.uniroma2.sdcc.flinkUtil.SortWindowFunction;
import it.uniroma2.sdcc.flinkUtil.Tuple11AverageAggregateFunction;
import it.uniroma2.sdcc.flinkUtil.Tuple2AverageAggregateFunction;
import it.uniroma2.sdcc.serializer_deserializer.TrafficLightDeserializationSchema;
import it.uniroma2.sdcc.serializer_deserializer.Tuple2Serializer;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.*;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.dropwizard.metrics.DropwizardMeterWrapper;
import org.apache.flink.metrics.Gauge;
import org.apache.flink.metrics.Meter;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.AssignerWithPeriodicWatermarks;
import org.apache.flink.streaming.api.functions.AssignerWithPunctuatedWatermarks;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.co.CoFlatMapFunction;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.watermark.Watermark;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;

import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer011;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer011;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.apache.kafka.clients.consumer.ConsumerConfig;

import javax.annotation.Nullable;
import java.util.*;

/*
    Returns the most dangerous intersections on the map based on vehicles crossing speed.
    As a result it provides the rank computed every 5, 10 and 30 minutes.
 */

public class FlinkCore {

    public static void main(String[] args) {
        //Flink run configurations
        Properties configProperties = new Properties();
        configProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                "kafka.cini-project.cloud:9092"); //kafka bootstrap server address
        configProperties.setProperty("group.id", "a");
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.enableCheckpointing(5000, CheckpointingMode.EXACTLY_ONCE); //Kafka semantic for Flink reads
        env.setParallelism(2);
        env.getConfig().setLatencyTrackingInterval(1000L);

        //Register a consumer on topic 'light' reading traffic light tuples
        FlinkKafkaConsumer011<Tuple11<Integer, Integer, Double, Double, Long,
                Double, Integer, Double, Boolean, Boolean, Boolean>> trafficLightTupleConsumer = new FlinkKafkaConsumer011<>("light", new TrafficLightDeserializationSchema(), configProperties);
        trafficLightTupleConsumer.setStartFromLatest();
        DataStream<Tuple11<Integer, Integer, Double, Double, Long,
                Double, Integer, Double, Boolean, Boolean, Boolean>> dataStream = env.addSource(trafficLightTupleConsumer);

        dataStream = dataStream.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<Tuple11<Integer, Integer, Double, Double, Long, Double, Integer, Double, Boolean, Boolean, Boolean>>(Time.seconds(0)) {
            @Override
            public long extractTimestamp(Tuple11<Integer, Integer, Double, Double, Long, Double, Integer, Double, Boolean, Boolean, Boolean> tuple11) {
                return tuple11.f4;
            }
        });


        //for metric computing only
        dataStream.process(new ProcessFunction<Tuple11<Integer, Integer, Double, Double, Long, Double, Integer, Double, Boolean, Boolean, Boolean>, Object>() {
            private transient Meter meter;

            @Override
            public void open(Configuration parameters) throws Exception {
                com.codahale.metrics.Meter dropwizard = new com.codahale.metrics.Meter();
                this.meter = getRuntimeContext().getMetricGroup().addGroup("Query1").meter("throughput_in", new DropwizardMeterWrapper(dropwizard));
            }

            @Override
            public void processElement(Tuple11<Integer, Integer, Double, Double, Long, Double, Integer, Double, Boolean, Boolean, Boolean> tuple11, Context context, Collector<Object> collector) throws Exception {
                this.meter.markEvent();
            }
        });

        //5 minutes intersection average speed
        DataStream<Tuple2<Integer, Double>> stream5mins = dataStream
                .keyBy(0)
                .timeWindow(Time.minutes(5))
                .aggregate(new Tuple11AverageAggregateFunction(), new AverageProcessFunction());


        //10 minutes intersection average speed, computed with 5-minutes sliding window
        DataStream<Tuple2<Integer, Double>> stream10mins = stream5mins
                .keyBy(0)
                .timeWindow(Time.minutes(10), Time.minutes(5))
                .aggregate(new Tuple2AverageAggregateFunction(), new AverageProcessFunction());

        //30 minutes intersection average speed, computed with 10-minutes sliding window
        DataStream<Tuple2<Integer, Double>> stream30mins = stream10mins
                .keyBy(0)
                .timeWindow(Time.minutes(30), Time.minutes(10))
                .aggregate(new Tuple2AverageAggregateFunction(), new AverageProcessFunction());

        //5 minutes average speed rank
        stream5mins.timeWindowAll(Time.minutes(5))
                .apply(new SortWindowFunction())
                .addSink(new FlinkKafkaProducer011<Tuple2<Integer, Double>[]>("rank15mins", new Tuple2Serializer(), configProperties));
        stream5mins.print();

        //10 minutes average speed rank
        stream10mins.timeWindowAll(Time.minutes(5))
                .apply(new SortWindowFunction())
                .addSink(new FlinkKafkaProducer011<Tuple2<Integer, Double>[]>("rank60mins", new Tuple2Serializer(), configProperties));

        stream10mins.print();

        //30 minutes average speed rank
        stream30mins.timeWindowAll(Time.minutes(10))
                .apply(new SortWindowFunction())
                .addSink(new FlinkKafkaProducer011<Tuple2<Integer, Double>[]>("rank24hrs", new Tuple2Serializer(), configProperties));
        stream30mins.print();


        try

        {
            env.execute("Query 1");
        } catch (
                Exception e)

        {
            e.printStackTrace();

        }
    }
}


