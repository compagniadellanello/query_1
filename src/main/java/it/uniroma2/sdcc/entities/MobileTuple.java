package it.uniroma2.sdcc.entities;


import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import org.apache.flink.api.java.tuple.Tuple5;

import java.io.IOException;
import java.sql.Timestamp;

@Getter
@Setter
@ToString
public class MobileTuple{

    public Integer userID;
    private GeoCoordinate coordinate;
    private Timestamp timestamp;
    private Double speed;

    public MobileTuple() {
    }

    public MobileTuple(Integer userID, GeoCoordinate coordinate, Timestamp timestamp, Double speed) {
        this.userID = userID;
        this.coordinate = coordinate;
        this.timestamp = timestamp;
        this.speed = speed;
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }
/*
    public Integer toTuple5() {
        return this.userID;
    }
*/


    public Tuple5<Integer, Double, Double,Timestamp, Double> toTuple5() {
         return new Tuple5<>(this.userID, this.coordinate.getLatitude(), this.coordinate.getLongitude(), this.timestamp, this.speed);
    }

    public static MobileTuple parseMobileTuple(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD,JsonAutoDetect.Visibility.ANY);
        return objectMapper.readValue(json, MobileTuple.class);
    }

}
