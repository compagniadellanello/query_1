package it.uniroma2.sdcc.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString

/*
    Output class for the first query. Every entry of the rank contains an intersection Id an average vehicle speed
 */
public class Position implements Serializable {

    private Integer ID;
    private Double speed;

    public Position(Integer ID, Double speed) {
        this.ID = ID;
        this.speed = speed;
    }

    public Position() {
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }
}
