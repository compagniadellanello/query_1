package it.uniroma2.sdcc.entities;

public class GeoCoordinate {

    private Double Latitude, Longitude;

    public GeoCoordinate() {
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }
}
