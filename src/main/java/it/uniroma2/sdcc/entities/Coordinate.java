package it.uniroma2.sdcc.entities;

import lombok.*;

@Getter
@Setter
public class Coordinate {
    private double x;
    private double y;


    public Coordinate(double latitude, double longitude) {
        this.x = latitude;
        this.y = longitude;
    }


    public Coordinate() {
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
