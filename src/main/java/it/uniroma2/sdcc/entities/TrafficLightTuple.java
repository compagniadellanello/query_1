package it.uniroma2.sdcc.entities;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.flink.api.java.tuple.Tuple11;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Arrays;

@Getter
@Setter

/*
    Class to abstract the tuple emitted by every traffic light
 */
public class TrafficLightTuple{
    private Integer crossID, semID, vehiclesNum;
    private GeoCoordinate coordinate;
    private Timestamp timestamp;
    private Double greenLightDuration, avgVehicleSpeed;
    private Boolean lightsStatus[] =  new Boolean[3];

    public TrafficLightTuple(Integer crossID, Integer semID, Integer vehiclesNum, GeoCoordinate coordinate, Timestamp timestamp, Double greenLightDuration, Double avgVehicleSpeed) {
        this.crossID = crossID;
        this.semID = semID;
        this.vehiclesNum = vehiclesNum;
        this.coordinate = coordinate;
        this.timestamp = timestamp;
        this.greenLightDuration = greenLightDuration;
        this.avgVehicleSpeed = avgVehicleSpeed;
        Arrays.fill(this.lightsStatus, Boolean.TRUE);
    }

    public String toJson() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(this);
    }

    public static TrafficLightTuple parseTrafficLightTuple(String json) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, TrafficLightTuple.class);
    }
    public Tuple11<Integer, Integer, Double, Double, Long,
                Double, Integer, Double, Boolean, Boolean, Boolean> toTuple11(){
        return new Tuple11<>(this.crossID, this.semID, this.coordinate.getLatitude(), this.coordinate.getLongitude(),
                this.timestamp.getTime(), this.greenLightDuration, this.vehiclesNum, this.avgVehicleSpeed, this.lightsStatus[0],
                this.lightsStatus[1],this.lightsStatus[2]);
    }

    public TrafficLightTuple() {
    }

    public Integer getCrossID() {
        return crossID;
    }

    public void setCrossID(Integer crossID) {
        this.crossID = crossID;
    }

    public Integer getSemID() {
        return semID;
    }

    public void setSemID(Integer semID) {
        this.semID = semID;
    }

    public Integer getVehiclesNum() {
        return vehiclesNum;
    }

    public void setVehiclesNum(Integer vehiclesNum) {
        this.vehiclesNum = vehiclesNum;
    }

    public GeoCoordinate getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(GeoCoordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Double getGreenLightDuration() {
        return greenLightDuration;
    }

    public void setGreenLightDuration(Double greenLightDuration) {
        this.greenLightDuration = greenLightDuration;
    }

    public Double getAvgVehicleSpeed() {
        return avgVehicleSpeed;
    }

    public void setAvgVehicleSpeed(Double avgVehicleSpeed) {
        this.avgVehicleSpeed = avgVehicleSpeed;
    }

    public Boolean[] getLightsStatus() {
        return lightsStatus;
    }

    public void setLightsStatus(Boolean[] lightsStatus) {
        this.lightsStatus = lightsStatus;
    }
}
