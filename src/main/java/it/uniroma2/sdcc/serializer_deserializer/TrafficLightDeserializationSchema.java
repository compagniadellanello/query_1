package it.uniroma2.sdcc.serializer_deserializer;

import it.uniroma2.sdcc.entities.Coordinate;
import it.uniroma2.sdcc.entities.MobileTuple;
import it.uniroma2.sdcc.entities.TrafficLightTuple;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple11;
import org.apache.flink.api.java.tuple.Tuple13;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.typeutils.TupleTypeInfo;

import java.io.IOException;
import java.sql.Timestamp;

public class TrafficLightDeserializationSchema implements DeserializationSchema<Tuple11<Integer, Integer, Double, Double, Long,
        Double, Integer, Double, Boolean, Boolean, Boolean>> {
    private static final long serialVersionUID = 6154188370181669758L;


    @Override
    public Tuple11<Integer, Integer, Double, Double, Long,
            Double, Integer, Double, Boolean, Boolean, Boolean> deserialize(byte[] message) throws IOException {
        String json = new String(message);
        TrafficLightTuple trafficLightTuple = TrafficLightTuple.parseTrafficLightTuple(json);
        return trafficLightTuple.toTuple11();
    }

    @Override
    public boolean isEndOfStream(Tuple11<Integer, Integer, Double, Double, Long,
            Double, Integer, Double, Boolean, Boolean, Boolean> nextElement) {
        return false;
    }

    @Override
    public TypeInformation<Tuple11<Integer, Integer, Double, Double, Long,
            Double, Integer, Double, Boolean, Boolean, Boolean>> getProducedType() {
        return new TupleTypeInfo<>(TypeInformation.of(Integer.class),
                TypeInformation.of(Integer.class),
                TypeInformation.of(Double.class),
                TypeInformation.of(Double.class),
                TypeInformation.of(Long.class),
                TypeInformation.of(Double.class),
                TypeInformation.of(Integer.class),
                TypeInformation.of(Double.class),
                TypeInformation.of(Boolean.class),
                TypeInformation.of(Boolean.class),
                TypeInformation.of(Boolean.class));
    }

}
