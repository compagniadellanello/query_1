package it.uniroma2.sdcc.serializer_deserializer;

import it.uniroma2.sdcc.entities.Position;
import org.apache.flink.api.common.serialization.SerializationSchema;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.flink.streaming.util.serialization.KeyedSerializationSchema;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Tuple2Serializer implements SerializationSchema<Tuple2<Integer, Double>[]> {
    @Override
    public byte[] serialize(Tuple2<Integer, Double>[] tuple) {
        ObjectMapper objectMapper = new ObjectMapper();
        byte[] bytes = new byte[0];
        List<Position> list = new ArrayList<>();
        for (Tuple2 t: tuple) {
            list.add(new Position((Integer) t.f0, (Double) t.f1));

        }
        try {
            bytes = objectMapper.writeValueAsBytes(list);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return bytes;
    }
}
