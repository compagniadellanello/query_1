package it.uniroma2.sdcc.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.*;

public class KafkaConnectorConsumer <T,S> {

    private KafkaConsumer<T, S> kafkaConsumer;

    public KafkaConnectorConsumer(String server, String topic, String group_id) {
        Properties configProperties = new Properties();
        configProperties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                server);
        configProperties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());
        configProperties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());
        configProperties.put(ConsumerConfig.GROUP_ID_CONFIG, group_id);
        //configProperties.put(ConsumerConfig.CLIENT_ID_CONFIG, "simple");
        kafkaConsumer = new KafkaConsumer<T, S>(configProperties);
        kafkaConsumer.subscribe(Collections.singleton(topic));
    }

    public void subscribe(String topic) {
        kafkaConsumer.subscribe(Collections.singleton(topic));
    }

    public HashMap<String, ArrayList<S>> consume() {
        ConsumerRecords<T, S> consumerRecords = kafkaConsumer.poll(100);
        HashMap<String, ArrayList<S>> map = new HashMap<String, ArrayList<S>>();
        Set<String> set = kafkaConsumer.listTopics().keySet();
        for (String topic: set){
            Iterable<ConsumerRecord<T, S>> consumerRecord = consumerRecords.records(topic);
            ArrayList<S> arrayList = new ArrayList<S>();
            for (ConsumerRecord<T, S> record : consumerRecord) {
                arrayList.add(record.value());
            }
            map.put(topic, arrayList);
        }
        return map;
    }

}
