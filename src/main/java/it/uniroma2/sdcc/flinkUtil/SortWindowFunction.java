package it.uniroma2.sdcc.flinkUtil;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.PriorityQueue;

/*
    Sorts the previous output stream and selects the ten biggest values, in descending order
 */
public class SortWindowFunction implements AllWindowFunction<Tuple2<Integer,Double>, Tuple2<Integer, Double>[], TimeWindow> {


    @Override
    public void apply(TimeWindow timeWindow, Iterable<Tuple2<Integer, Double>> iterable, Collector<Tuple2<Integer, Double>[]> collector) throws Exception {
        PriorityQueue<Tuple2<Integer, Double>> array = new PriorityQueue<>(1, (o1, o2) -> o2.f1.compareTo(o1.f1));
        iterable.forEach(i -> array.add(i));
        Tuple2<Integer, Double>[] out = new Tuple2[10];
        for (int i = 0; i < 10; i++){
            Tuple2<Integer, Double> t = array.poll();
            if (t != null) {
                out[i] = t;
            } else {
                out[i] = new Tuple2<>(0, 0.0);
            }
        }
        collector.collect(out);
    }
}
