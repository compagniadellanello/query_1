package it.uniroma2.sdcc.flinkUtil;

import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.dropwizard.metrics.DropwizardMeterWrapper;
import org.apache.flink.dropwizard.metrics.FlinkMeterWrapper;
import org.apache.flink.metrics.Meter;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;

/*
    Class used to compute the throughput for the first query
 */
public class AverageProcessFunction extends ProcessWindowFunction<Object, Tuple2<Integer, Double>, Tuple, TimeWindow> {


    private transient Meter meter;

    @Override
    public void open(Configuration parameters) throws Exception {
        com.codahale.metrics.Meter dropwizard = new com.codahale.metrics.Meter();
        this.meter = getRuntimeContext().getMetricGroup().addGroup("Query1").meter("throughput", new DropwizardMeterWrapper(dropwizard));
    }

    @Override
    public void process(Tuple tuple, Context context, Iterable<Object> iterable, Collector<Tuple2<Integer, Double>> collector) throws Exception {
        this.meter.markEvent();
        collector.collect(((Tuple2<Integer, Double>) iterable.iterator().next()));
    }
}
