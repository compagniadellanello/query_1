package it.uniroma2.sdcc.flinkUtil;

import org.apache.flink.api.common.functions.AggregateFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;


public class Tuple2AverageAggregateFunction implements AggregateFunction<Tuple2<Integer, Double>, Tuple3<Integer, Double, Integer>, Object> {

    @Override
    public Tuple3<Integer, Double, Integer> createAccumulator() {
        return new Tuple3<>(0, 0.0, 0);
    }

    @Override
    public Tuple3<Integer, Double, Integer> add(Tuple2<Integer, Double> tuple, Tuple3<Integer, Double, Integer> aggr) {
        return new Tuple3<>(tuple.f0, aggr.f1 + tuple.f1, aggr.f2 + 1);
    }

    @Override
    public Tuple2<Integer, Double> getResult(Tuple3<Integer, Double, Integer> tuple) {
        return new Tuple2<>(tuple.f0, tuple.f1 / tuple.f2);
    }

    @Override
    public Tuple3<Integer, Double, Integer> merge(Tuple3<Integer, Double, Integer> acc1, Tuple3<Integer, Double, Integer> acc2) {
        return new Tuple3<>(acc1.f0, acc1.f1 + acc2.f1, acc1.f2 + acc2.f2);
    }
}
